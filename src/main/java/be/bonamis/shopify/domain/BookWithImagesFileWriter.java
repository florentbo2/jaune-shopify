package be.bonamis.shopify.domain;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class BookWithImagesFileWriter extends FileWriter<BookWithImagesFileWriter.BookWithImages> {

    public static final String COUVERTURES_PREFIX = "https://www.bedetheque.com/media/Couvertures/Couv_";

    public BookWithImagesFileWriter(String filePath, String[] columns, Set<BookWithImages> books) {
        super(filePath, columns, books);
    }

    @Override
    void writeRows(Sheet sheet, Set<BookWithImages> books) {
        int rowNum = 1;

        for (BookWithImages book : books) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(book.getIsbn());
            row.createCell(1).setCellValue(book.getTitle());
            row.createCell(2).setCellValue(book.getDesc());
            row.createCell(3).setCellValue(book.getImage());
            row.createCell(4).setCellValue(book.getPlanche());
            row.createCell(5).setCellValue(book.getVerso());
        }

    }

    @Getter
    @SuperBuilder
    static class BookWithImages extends BooksSample.Book {
        private String planche;
        private String verso;
    }

    @Getter
    static class ExtraImages {
        private String planche;
        private String verso;

        public ExtraImages(String image) {
            String plancheUrl = image != null ? image.replace(COUVERTURES_PREFIX, "https://www.bedetheque.com/media/Planches/PlancheA_") : "";
            String versoUrl = image != null ? image.replace(COUVERTURES_PREFIX, "https://www.bedetheque.com/media/Versos/Verso_") : "";

            this.planche = (image != null && image.contains(COUVERTURES_PREFIX)) ? isImageExist(plancheUrl) : "";
            this.verso = (image != null && image.contains(COUVERTURES_PREFIX)) ? isImageExist(versoUrl) : "";
        }

        /*

         */
    }

    public static void main(String[] args) throws IOException {
        String input = "/home/florent/Documents/aws/books_bdgest02.xlsx";
        String output = "/home/florent/Documents/aws/books_bdgest02_with_other_images.xlsx";


        try (InputStream inp = new FileInputStream(input)) {
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);

            Iterator<Row> sourceIterator = sheet.iterator();

            Iterable<Row> iterable = () -> sourceIterator;
            Stream<Row> targetStream = StreamSupport.stream(iterable.spliterator(), false);

            Set<BookWithImages> books = targetStream.skip(1).map(row -> {
                String isbn = row.getCell(0).getStringCellValue();
                String title = row.getCell(1).getStringCellValue();
                String desc = row.getCell(2).getStringCellValue();
                String image = row.getCell(3).getStringCellValue();

                ExtraImages extraImages = new ExtraImages(image);


                //return new BookWithImages(isbn, title, desc, image, planche, verso);
                return BookWithImages.builder()
                        .isbn(isbn)
                        .title(title)
                        .desc(desc)
                        .image(image)
                        .planche(extraImages.getPlanche())
                        .verso(extraImages.getVerso())
                        .build();

            }).collect(Collectors.toCollection(LinkedHashSet::new));

            String[] columns = {"ISBN", "Title", "Descr", "Image", "planche", "verso"};
            new BookWithImagesFileWriter(output, columns, books).writeFile();
        }
    }

    private static String isImageExist(String imageUrl) {
        try {
            final URL url = new URL(imageUrl);
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            int responseCode = huc.getResponseCode();
            return responseCode == 200 ? imageUrl : "";
            //System.out.println(responseCode);
            // Handle response code here...
        } catch (Exception uhe) {
            return "";
            // Handle exceptions as necessary
        }
    }
}
