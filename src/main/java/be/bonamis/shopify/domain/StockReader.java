package be.bonamis.shopify.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static be.bonamis.shopify.domain.BabelioJsoup.getBabelioBook;
import static be.bonamis.shopify.domain.CSVWriter.STOCK_PATH;
import static be.bonamis.shopify.domain.DecitreJsoup.getDecitreImage;
import static be.bonamis.shopify.domain.ShopifyBookFileWriter.getStringCellValue;

public class StockReader {

    public static void main(String[] args) throws Exception {
        Map<String, JauneStockBook> jauneStockBookMap = new StockReader().read(STOCK_PATH);
        System.out.println(jauneStockBookMap.size());

        jauneStockBookMap.entrySet().forEach(new Consumer<Map.Entry<String, JauneStockBook>>() {
            @Override
            public void accept(Map.Entry<String, JauneStockBook> stockBookEntry) {
                String isbn = stockBookEntry.getKey();
                System.out.println(getBabelioBook(isbn).getDesc());
                System.out.println(getDecitreImage(isbn));
            }
        });



    }

    Map<String, JauneStockBook> read(String path) throws Exception {
        try (InputStream inp = new FileInputStream(path)) {
            Sheet sheet = WorkbookFactory.create(inp).getSheetAt(0);

            Iterator<Row> sourceIterator = sheet.iterator();

            Iterable<Row> iterable = () -> sourceIterator;
            Stream<Row> targetStream = StreamSupport.stream(iterable.spliterator(), false);

            Stream<Row> rowStream = targetStream.skip(1)
                    //.limit(20)
                    ;

            return rowStream.collect(Collectors.toMap(
                                                    row -> getStringCellValue(row, 0),
                                                    row -> JauneStockBook.builder()
                                                            .isbn(getStringCellValue(row, 0))
                                                            .title(getStringCellValue(row, 1))
                                                            .author(getStringCellValue(row, 2))
                                                            //.categoryCode((int) row.getCell(6).getNumericCellValue())
                                                            .categoryCode(getStringCellValue(row, 6))
                                                            .inventory((int) row.getCell(2).getNumericCellValue())
                                                            .build()
            ));


        }
    }

    @Getter
    @Builder
    @ToString
    public static class JauneStockBook {
        private String isbn;
        private String title;
        private String author;
        private String categoryCode;
        private int inventory;

    }
}
