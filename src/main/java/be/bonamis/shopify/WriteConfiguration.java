package be.bonamis.shopify;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class WriteConfiguration {

    @Value("${report.export.path}")
    private String exportPath;

    @Value("${dry.run:false}")
    private boolean isDryRunExecution;

    @Bean
    public WriteService emailService() {
        return new WriteService(exportPath, isDryRunExecution);
    }
}
