package be.bonamis.shopify.domain;

/*
 * Copyright (c) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * A sample application that demonstrates how Google Books Client Library for
 * Java can be used to query Google Books. It accepts queries in the command
 * line, and prints the results to the console.
 * <p>
 * $ java com.google.sample.books.BooksSample [--author|--isbn|--title] "<query>"
 * <p>
 * Please start by reviewing the Google Books API documentation at:
 * http://code.google.com/apis/books/docs/getting_started.html
 */
public class BooksSample {

    /**
     * Be sure to specify the name of your application. If the application name is {@code null} or
     * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
     */
    private static final String APPLICATION_NAME = "Books-description-images";

    /*public static void waitForPageLoaded(WebDriver driver) {
        ExpectedCondition<Boolean> expectation = driver1 -> {
            assert driver1 != null;
            return ((JavascriptExecutor) driver1).executeScript("return document.readyState").toString().equals("complete");
        };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            System.out.println("Timeout waiting for Page Load Request to complete.");
        }
    }

    private static String getAmzDescription(String isbn, WebDriver driver) {

        driver.get("https://www.amazon.fr/dp/" + isbn);
        waitForPageLoaded(driver);
        driver.switchTo().frame("bookDesc_iframe");
        String description = driver.findElement(By.id("iframeContent")).getText();
        System.out.println(description);
        return description;

    }

    static Book queryGoogleBooks(JsonFactory jsonFactory, String query, String isbn, WebDriver driver) throws Exception {
        Book book = Book.builder()
                .isbn(isbn)
                .title("")
                .desc("")
                .build();

        // Set up Books client.
        final Books books = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, null)
                .setApplicationName(APPLICATION_NAME)
                .setGoogleClientRequestInitializer(new BooksRequestInitializer("AIzaSyCRF0XM8X_0k5mqDNwtAl_o2DL46KlBSf0"))
                .build();
        // Set query string and filter only Google eBooks.
        //System.out.println("Query: [" + query + "]");
        List volumesList = books.volumes().list(query);
        //volumesList.setFilter("ebooks");

        // Execute the query.
        Volumes volumes = volumesList.execute();
        if (volumes.getTotalItems() == 0 || volumes.getItems() == null) {
            System.out.println("No matches found.");
            //return;
        } else {
            // Output results.
            for (Volume volume : volumes.getItems()) {
                Volume.VolumeInfo volumeInfo = volume.getVolumeInfo();
                if (volumeInfo.getDescription() != null && volumeInfo.getDescription().length() > 0) {
                    //System.out.println("Description: " + volumeInfo.getDescription());
                    book.setDesc(volumeInfo.getDescription());
                } else {
                    String infoLink = volumeInfo.getInfoLink();
                    Document doc = Jsoup.connect(infoLink).get();
                    String description = doc.select("meta[name=description]").first().attr("content");
                    if (description != null && description.length() > 0) {
                        book.setDesc(description);
                    } else {
                        volumeInfo.getIndustryIdentifiers()
                                .stream()
                                .filter(industryIdentifiers -> industryIdentifiers.getType().equals("ISBN_10"))
                                .findFirst()
                                .map(Volume.VolumeInfo.IndustryIdentifiers::getIdentifier)
                                .ifPresent(isbn_10 -> book.setDesc(getAmzDescription(isbn_10, driver)));
                    }
                }
                book.setTitle(volumeInfo.getTitle());

                *//*System.out.println("Title: " + volumeInfo.getTitle());
                System.out.println("getInfoLink: " + infoLink);
                System.out.println("getPreviewLink: " + volumeInfo.getPreviewLink());
                System.out.println("getCanonicalVolumeLink: " + volumeInfo.getCanonicalVolumeLink());*//*
                volumeInfo
                        .getIndustryIdentifiers()
                        .forEach(industryIdentifiers ->
                                System.out.println(industryIdentifiers.getType() + " - "
                                        + industryIdentifiers.getIdentifier()));


            }
        }
        System.out.println("==========");
        System.out.println(
                volumes.getTotalItems() + " total results at http://books.google.com/ebooks?q="
                        + URLEncoder.encode(query, "UTF-8"));
        return book;
    }*/

/*
    public static void main(String[] args) throws IOException {

        WebDriver driver = getWebDriver();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        */
/*

        String input = "9782344020630 ;  9782364807143 ; 9782749940588 ; " +
                "9782226257017 ; 9782889230129 ; 9791034738014";
*//*

        Set<Book> books = new LinkedHashSet<>();


        try (InputStream inp = new FileInputStream("/home/florent/Documents/aws/covid-intersticeasbl27mars-bdjeunesse.xls")) {
//InputStream inp = new FileInputStream("workbook.xlsx");
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);

            for (int i = 1; i < 5; i++) {
                Row row = sheet.getRow(i);
                String isbn = row.getCell(0).getStringCellValue();

                System.out.println(isbn);
                try {
                    //String query = "9782331046544";
                    //String query = "9782344020630";// la bombe
                    //String query = "9791034738014";// poilu
                    String prefix = "isbn:";
                    String query = prefix + isbn;
                    try {
                        books.add(queryGoogleBooks(jsonFactory, query, isbn, driver));
                        books.add(getBDgestBook(isbn, driver));
                        // Success!
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }

            */
/*for (Row row : sheet) {



            }*//*



        }

        BookFileWriter bookFileWriter
                = new BookFileWriter("/home/florent/Documents/aws/books_bdgest03.xlsx", columns, books);
        bookFileWriter.writeFile();
        driver.close();
        System.exit(0);
    }
*/

    /*static WebDriver getWebDriver() {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        WebDriver driver = new ChromeDriver();

        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury Tours";

        driver.get(baseUrl);

        String actualTitle = driver.getTitle();

        if (actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
        return driver;
    }*/
/*
    static Book getBDgestBook(String isbn, WebDriver driver) {
        Book book = Book.builder()
                .isbn(isbn)
                .build();

        driver.get("https://www.bedetheque.com/");
        long timeoutInSeconds = 2;
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        //waitForPageLoaded(driver);

        try {


            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("RechISBN")));

            WebElement rechISBN = driver.findElement(By.name("RechISBN"));
            rechISBN.sendKeys(isbn);
            driver.findElement(By.xpath("//input[@type='submit' and @value='Rechercher dans les albums']")).click();
            WebElement searchResult = driver.findElement(By.className("image-tooltip"));
            //System.out.println(searchResult.getAttribute("rel"));
            String href = searchResult.getAttribute("href");
            //System.out.println(href);
            Document doc = Jsoup.connect(href).get();
            String title = doc.title();
            String description = doc.select("meta[name=description]").first().attr("content");
            String image = doc.select("meta[property=og:image]").first().attr("content");

            book.setTitle(title);
            book.setDesc(description);
            book.setImage(image);
        } catch (Exception e) {
            e.printStackTrace();
        }



           */
/* //book.setTitle(title);
            System.out.println(title);
            System.out.println(description);
            System.out.println(image);*//*


        return book;
    }
*/

    private static String[] columns = {"ISBN", "Title", "Descr", "Image"};

    @Setter
    @Getter
    @SuperBuilder
    @ToString
    public static class Book {
        private String isbn;
        private String title;
        private String desc;
        private String image;
    }
}