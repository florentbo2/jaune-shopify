package be.bonamis.shopify.domain;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class DecitreJsoup {
    public static void main(String[] args) {
        String imageSrc = getDecitreImage("9782330124205");
        System.out.println(imageSrc);
    }

    public static String getDecitreImage(String isbn)  {
        try {
            Connection.Response response =
                    Jsoup.connect("https://www.decitre.fr/rechercher/result?q=" + isbn)
                            .timeout(10 * 1000)
                            .method(Connection.Method.GET)
                            .followRedirects(true)
                            .execute();

            Document document = response.parse();
            Elements visualContainer = document.select(".visual-container source");

            String type = visualContainer.stream()
                    .filter(element -> element.attr("type").equals("image/jpeg"))
                    .findFirst()
                    .map(element -> element.attr("data-srcset"))
                    .orElse(null);

           //System.out.println(visualContainer);
            //System.out.println("++++++");

            return (type != null ? type.split("jpg") : new String[0])[0] + "jpg";
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }
}
