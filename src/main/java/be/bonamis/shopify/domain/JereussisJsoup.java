package be.bonamis.shopify.domain;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class JereussisJsoup {
    public static void main(String[] args) {
        String imageSrc = getJereussisImage("9782875460929");
        System.out.println(imageSrc);
    }

    public static String getJereussisImage(String isbn)  {
        try {
            Document document = getDocument("http://jereussis.be/?s=" + isbn);

            Element moreLink = document.select(".more-link").first();
            String href = moreLink.attr("href");
            Document productDocument = getDocument(href);

            Elements select = productDocument.select(".elementor-image img");
/*            System.out.println(select.first().attr("src"));

            //elementor-image*/
            return select.first().attr("src");

        } catch (Exception e) {
            return null;
        }
    }

    private static Document getDocument(String url) throws IOException {
        Connection.Response response =
                Jsoup.connect(url)
                        .timeout(10 * 1000)
                        .method(Connection.Method.GET)
                        .followRedirects(true)
                        .execute();

        return response.parse();
    }
}
