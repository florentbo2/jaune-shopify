package be.bonamis.shopify.domain;

import be.bonamis.shopify.domain.BooksSample.Book;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.*;

import java.io.IOException;
import java.util.function.Consumer;

public class BdGest {

    public static void main(String[] args) throws IOException {
        BdGest bdGest = new BdGest();
        /*System.out.println(bdGest.getBDgestBook("9791034738014", getWebDriver()).getImage());
        getWebDriver().close();*/
        Book book = Book.builder()
                .isbn("isbn")
                .build();
        bdGest.setBookAttributes(book, "https://www.bedetheque.com/BD-Petit-Poilu-Tome-23-Duel-de-bulles-375249.html");

    }

    static WebDriver getWebDriver() {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        WebDriver driver = new ChromeDriver();

        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury Tours";

        driver.get(baseUrl);

        String actualTitle = driver.getTitle();

        if (actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
        return driver;
    }


    Book getBDgestBook(String isbn, WebDriver driver) {
        Book book = Book.builder()
                .isbn(isbn)
                .build();

        driver.get("https://www.bedetheque.com/");
        long timeoutInSeconds = 2;
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        //waitForPageLoaded(driver);

        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("RechISBN")));

            WebElement rechISBN = driver.findElement(By.name("RechISBN"));
            rechISBN.sendKeys(isbn);
            driver.findElement(By.xpath("//input[@type='submit' and @value='Rechercher dans les albums']")).click();
            WebElement searchResult = driver.findElement(By.className("image-tooltip"));
            //System.out.println(searchResult.getAttribute("rel"));
            String href = searchResult.getAttribute("href");
            System.out.println("href: " + href);
            setBookAttributes(book, href);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return book;
    }

    private void setBookAttributes(Book book, String href) throws IOException {
        Document doc = Jsoup.connect(href).get();
        String title = doc.title();
        String description = doc.select("meta[name=description]").first().attr("content");
        String image = doc.select("meta[property=og:image]").first().attr("content");

        doc.select(".infos-albums li").forEach(new Consumer<Element>() {
            @Override
            public void accept(Element element) {
                System.out.println("++++");
                System.out.println(element);
            }
        });
        System.out.println(image);
        book.setTitle(title);
        book.setDesc(description);
        book.setImage(image);
    }
}
