package be.bonamis.shopify.domain;

import be.bonamis.shopify.domain.BookWithImagesFileWriter.BookWithImages;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.apache.http.util.TextUtils;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ShopifyBookFileWriter extends FileWriter<ShopifyBookFileWriter.ShopifyBook> {

    public ShopifyBookFileWriter(String filePath, Set<ShopifyBook> books) {
        super(filePath, columns, books);
    }


    public static String[] columns = {
            "Handle"
            , "Title"
            , "Body (HTML)"
            , "Vendor"
            , "Type"
            , "Tags"
            , "Option1 Name"
            , "Option1 Value"
            , "Variant Grams"
            , "Variant Inventory Tracker"
            , "Variant Inventory Qty"
            , "Variant Inventory Policy"
            , "Variant Fulfillment Service"
            , "Variant Price"
            , "Variant Barcode"
            , "Image Src"
            , "Image position"
            , "Variant Weight Unit"
    };

    @Override
    void writeRows(Sheet sheet, Set<ShopifyBook> books) {
        int rowNum = 1;

        for (ShopifyBook book : books) {
            Row row = sheet.createRow(rowNum++);

            /*
        Handle
        Title
        Body (HTML)
        Vendor
        Type
        Tags
        Option1 Name
        Option1 Value
        Variant Grams
        Variant Inventory Tracker
        Variant Inventory Qty
        Variant Inventory Policy
        	Variant Fulfillment Service
        	Variant Price
        	Variant Barcode
        	Image Src
        	Image Position
        	Variant Weight Unit
         */

            row.createCell(0).setCellValue(book.getHandle());
            row.createCell(1).setCellValue(book.getTitle());
            row.createCell(2).setCellValue(book.getDesc());
            row.createCell(3).setCellValue(book.getVendor());
            row.createCell(4).setCellValue(book.getType());
            row.createCell(5).setCellValue(book.getTags());
            row.createCell(6).setCellValue(book.getOption1Name());
            row.createCell(7).setCellValue(book.getOption1Value());
            row.createCell(8).setCellValue(book.getVariantGrams());
            row.createCell(9).setCellValue(book.getVariantInventoryTracker());
            row.createCell(10).setCellValue(book.getInventory());
            row.createCell(11).setCellValue(book.getVariantInventoryPolicy());
            row.createCell(12).setCellValue(book.getVariantFulfillmentService());
            row.createCell(13).setCellValue(book.getPrice());
            row.createCell(14).setCellValue(book.getIsbn());
            row.createCell(15).setCellValue(book.getImage());
            row.createCell(16).setCellValue(book.getImagePosition());
            row.createCell(17).setCellValue(book.getVariantWeightUnit());
        }
    }

    @Getter
    @SuperBuilder
    public static class ShopifyBook extends BookWithImages {
        String vendor;
        String inventory;
        String price;
        int imagePosition;
        String handle;
        String type;
        String tags;
        String option1Name;
        String option1Value;
        String variantGrams;
        String variantInventoryTracker;
        String variantInventoryPolicy;
        String variantFulfillmentService;
        String variantWeightUnit;

        /*
        Type
        Tags
        Option1 Name
        Option1 Value
        Variant Grams
        Variant Inventory Tracker
        Variant Inventory Policy
        	Variant Fulfillment Service
        	Variant Weight Unit
         */
    }

    public static void main(String[] args) throws Exception {
        String input = "/home/florent/Documents/aws/bd_the_final_one.xlsx";
        String output = "/home/florent/Documents/aws/bd_jeunesse_the_shopify_final_one.xlsx";

        try (InputStream inp = new FileInputStream(input)) {
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);

            Iterator<Row> sourceIterator = sheet.iterator();

            Iterable<Row> iterable = () -> sourceIterator;
            Stream<Row> targetStream = StreamSupport.stream(iterable.spliterator(), false);
            LinkedHashSet<ShopifyBook> shopifyBooks = targetStream
                    .skip(1)
                    .filter(row -> row.getCell(0) != null && row.getCell(0).getStringCellValue().length() > 0)
                    //.limit(50)
                    .flatMap((Function<Row, Stream<ShopifyBook>>) row -> {

                        String planche = getStringCellValue(row, 8);
                        String verso = getStringCellValue(row, 9);
                        Set<ShopifyBook> shopifyBooks1 = new LinkedHashSet<>();
                        String handle = getStringCellValue(row, 0);
                        try {
                            ShopifyBook shopifyBook = ShopifyBook.builder()
                                    .handle(handle)
                                    .title(getStringCellValue(row, 1))
                                    .desc(getStringCellValue(row, 2))
                                    .vendor(getStringCellValue(row, 3))
                                    .inventory(String.valueOf((int) row.getCell(4).getNumericCellValue()))
                                    .price(getStringCellValue(row, 5))
                                    .isbn(getStringCellValue(row, 6))
                                    .image(getStringCellValue(row, 7))
                                    .imagePosition(1)
                                    .type("Bande dessinée")
                                    .tags("bd, jeunesse")
                                    .option1Name("Title")
                                    .option1Value("Default Title")
                                    .variantGrams("0")
                                    .variantInventoryTracker("shopify")
                                    .variantInventoryPolicy("deny")
                                    .variantFulfillmentService("manual")
                                    .variantWeightUnit("kg")
                                    .build();

                            shopifyBooks1.add(shopifyBook);

                            addExtraImages(handle, planche, verso, shopifyBooks1, "bd, jeunesse");
                        } catch (Exception e) {

                            System.out.println(handle);
                            //e.printStackTrace();
                        }
                        return shopifyBooks1.stream();
                    }).collect(Collectors.toCollection(LinkedHashSet::new));


            /*
        Handle
        Title
        Body (HTML)
        Vendor
        Type
        Tags
        Option1 Name
        Option1 Value
        Variant Grams
        Variant Inventory Tracker
        Variant Inventory Qty
        Variant Inventory Policy
        	Variant Fulfillment Service
        	Variant Price
        	Variant Barcode
        	Image Src
        	Image Position
        	Variant Weight Unit
         */

            new ShopifyBookFileWriter(output, shopifyBooks).writeFile();
        }
    }

    static void addExtraImages(String handle, String planche, String verso, Set<ShopifyBook> shopifyBooks, String tags) {
        addExtraImage(handle, planche, 2, shopifyBooks, tags);
        addExtraImage(handle, verso, 3, shopifyBooks, "bd, jeunesse");
    }

    public static String getStringCellValue(Row row, int i) {
        try {
            Cell cell = row.getCell(i);
            return cell != null ? cell.getStringCellValue() : "";
        } catch (Exception e1) {
            return "";
        }
    }

    private static void addExtraImage(String handle, String otherImage, int imagePosition, Set<ShopifyBook> shopifyBooks, String tags) {
        if (!TextUtils.isBlank(otherImage)) {
            ShopifyBook shopifyBookPlanche = ShopifyBook.builder()
                    .handle(handle)
                    .title("")
                    .desc("")
                    .vendor("")
                    .inventory("")
                    .price("")
                    .isbn("")
                    .image(otherImage)
                    .imagePosition(imagePosition)
                    .type("")
                    .tags(tags)
                    .option1Name("")
                    .option1Value("")
                    .variantGrams("")
                    .variantInventoryTracker("")
                    .variantInventoryPolicy("")
                    .variantFulfillmentService("")
                    .variantWeightUnit("")
                    .build();
            shopifyBooks.add(shopifyBookPlanche);
        }
    }
}
