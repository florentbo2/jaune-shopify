package be.bonamis.shopify.domain;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.http.util.TextUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.StreamSupport;

public class CSVWriter {

    //static final String STOCK_PATH = "/home/florent/Documents/aws/shopify/in/Stock_20200404-bd-bdjeunesse-mangas-loisirs-scolaire-romans-poche-jeunesse.xls";
    static final String STOCK_PATH = "/home/florent/Documents/aws/shopify/in/webshop-stockromanjeunesse-6avril2020.xls";

    static final String STOCK_PATH_IN = "/home/florent/Documents/aws/shopify/stock/products_export_1 7.csv";
    static final String STOCK_PATH_OUT = "/home/florent/Documents/aws/shopify/out/inventory.csv";

    public static void main(String[] args) throws Exception {

        Map<String, StockReader.JauneStockBook> jauneStockBookMap = new StockReader().read(STOCK_PATH);

        try (
                Reader reader = Files.newBufferedReader(Paths.get(STOCK_PATH_IN));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        ) {
            try (
                    BufferedWriter writer = Files.newBufferedWriter(Paths.get(STOCK_PATH_OUT));

                    CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                            .withHeader("Handle", "Title", "SKU", "Librairie Jaune"));
            ) {
                //System.out.println(csvParser.getRecords().size());

                StreamSupport.stream(csvParser.spliterator(), false)
                        .skip(1)
                        /*.filter(csvRecord -> {
                            String title = csvRecord.get(1);
                            return !TextUtils.isBlank(title);
                        })*/
                        //.limit(10)
                        .forEach(csvRecord -> {
                            String handle = csvRecord.get(0);
                            String title = csvRecord.get(1);
                            String isbn = csvRecord.get(23).replace("'", "");
                            //System.out.println(isbn);

                            StockReader.JauneStockBook jauneStockBook = jauneStockBookMap.get(isbn);
                            //writeLine(csvPrinter, handle, title, isbn, 999);
                            if (!TextUtils.isBlank(isbn)) {
                                if (jauneStockBook != null) {
                                    int inventory = jauneStockBook.getInventory();
                                    writeLine(csvPrinter, handle, title, isbn, inventory);
                                } else {
                                    //writeLine(csvPrinter, handle, title, isbn, 0);
                                    System.out.println(title + " isbn: " + isbn);
                                }
                            }

                        });
                csvPrinter.flush();
            }
        }
    }

    private static void writeLine(CSVPrinter csvPrinter, String handle, String title, String isbn, int inventory) {
        try {
            csvPrinter.printRecord(handle, title, isbn, inventory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
