package be.bonamis.shopify;

import be.bonamis.shopify.domain.ShopifyBookFileWriter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static be.bonamis.shopify.domain.DecitreJsoup.getDecitreImage;
import static be.bonamis.shopify.domain.JereussisJsoup.getJereussisImage;
import static be.bonamis.shopify.domain.ShopifyBookFileWriter.getStringCellValue;

public class WriteService {

    private final String exportPath;
    private final boolean isDryRunExecution;

    public WriteService(String exportPath, boolean isDryRunExecution) {
        this.exportPath = exportPath;
        this.isDryRunExecution = isDryRunExecution;
    }


    void writeFile(String input, String output, String type, String tags) throws Exception {
        try (InputStream inp = new FileInputStream(this.exportPath + "/in/" + input)) {
            Sheet sheet = WorkbookFactory.create(inp).getSheetAt(0);

            Iterator<Row> sourceIterator = sheet.iterator();

            Iterable<Row> iterable = () -> sourceIterator;
            Stream<Row> targetStream = StreamSupport.stream(iterable.spliterator(), false);

            Stream<Row> rowStream = this.isDryRunExecution ? targetStream.skip(1).limit(5) : targetStream.skip(1);
            LinkedHashSet<ShopifyBookFileWriter.ShopifyBook> shopifyBooks = rowStream
                    .map(row -> {
                        String isbn = getStringCellValue(row, 0);
                        String title = getStringCellValue(row, 4);

                        String getDescription = "";
                        String image = getImage(isbn);
                        return getShopifyBook(type, tags, row, isbn, title, getDescription, image);

                    }).collect(Collectors.toCollection(LinkedHashSet::new));

            new ShopifyBookFileWriter(this.exportPath + "/out/" + output, shopifyBooks).writeFile();
        }
    }

    private String getImage(String isbn) {
        String decitreImage = getDecitreImage(isbn);
        return decitreImage != null ? decitreImage : getJereussisImage(isbn);
    }

    static ShopifyBookFileWriter.ShopifyBook getShopifyBook(String type, String tags, Row row, String isbn, String title, String getDescription, String image) {
        return ShopifyBookFileWriter.ShopifyBook.builder()
                .isbn(isbn)
                .inventory(getInventory(row))
                .price(getStringCellValue(row, 3))
                .title(title)
                .handle(title)
                .vendor(getStringCellValue(row, 5))
                .desc(getDescription)
                .image(image)
                .imagePosition(1)
                .type(type)
                .tags(tags)
                .option1Name("Title")
                .option1Value("Default Title")
                .variantGrams("0")
                .variantInventoryTracker("shopify")
                .variantInventoryPolicy("deny")
                .variantFulfillmentService("manual")
                .variantWeightUnit("kg")
                .build();
    }

    private static String getInventory(Row row) {
        try {
            return String.valueOf((int) row.getCell(2).getNumericCellValue());
        } catch (Exception e1) {
            return "";
        }
    }
}
