package be.bonamis.shopify.domain;

import com.shopify.ShopifySdk;
import com.shopify.model.ShopifyProduct;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Sdk2 {

    public static void main(String[] args) {
        ShopifySdk shopifySdk = ShopifySdk.newBuilder()
                .withSubdomain("librairie-jaune")
                .withAccessToken("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")
                .withMaximumRequestRetryTimeout(2, TimeUnit.SECONDS).withConnectionTimeout(1, TimeUnit.SECONDS).build();

        ShopifyProduct product = shopifySdk.getProduct("4729586647179");
        System.out.println(product.getTitle());

        Set<String> tags = product.getTags();
        System.out.println(tags);
    }
}
