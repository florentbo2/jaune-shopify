package be.bonamis.shopify;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class ShopifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopifyApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(WriteService writeService) {
        return (args) ->
        {

/*
Essai	essai
Manga	manga
Poche	poche
Soutien scolaire	soutien
Activités	activité
Albums	illustré
 */

            writeService.writeFile("webshop-stocksjeunesse-2avril2020.xls", "jeuness_shopify.xlsx", "Albums", "illustré");
            log.info("ending jeunesse");
            writeService.writeFile("webshop-stocksessaisloisirs-2avril2020.xls", "essai_shopify.xlsx", "Essai", "essai");
            log.info("ending Essai");
            writeService.writeFile("webshop-stockssoutienscolaire-2avril2020.xls", "soutien_shopify.xlsx", "Soutien scolaire", "soutien");
            log.info("ending soutien");
            log.info("ending application");
        };


    }


}
