package be.bonamis.shopify.domain;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class BabelioJsoup {

    public static final String BABELIO_COM = "https://www.babelio.com";

    public static void main(String[] args) {

        String isbn = "9782344020630";  // la bombe
        isbn = "9782882502605";//9782253146599
        isbn = "9782253146599";//9782253146599
        isbn = "9782021450354";//9782253146599
        isbn = "9782748514964";//9782253146599
        isbn = "9782330014476";//9782253146599

        System.out.println(getBabelioBook(isbn).getDesc());
    }

    static BooksSample.Book getBabelioBook(String isbn) {
        BooksSample.Book book = BooksSample.Book.builder()
                .isbn(isbn)
                .build();
        try {

            Response response =
                    Jsoup.connect("https://www.babelio.com/resrecherche.php")
                            .userAgent(getUserAgent(isbn))
                            .timeout(10 * 1000)
                            .method(Connection.Method.POST)
                            .data("Recherche", isbn)
                            .followRedirects(true)
                            .execute();

            Document document = response.parse();

            Elements title = document.select(".titre_livre");
            Element link = title.select("a").first();

            if (link != null) {


                String linkHref = link.attr("href"); // "http://example.com/"
                //System.out.println("href: " + linkHref);

                Response productResponse =
                        Jsoup.connect(BABELIO_COM + linkHref)
                                .userAgent(getUserAgent(isbn))
                                .timeout(10 * 1000)
                                .method(Connection.Method.GET)
                                //.data("Recherche", "9782344020630")
                                //.followRedirects(true)
                                .execute();

                Document productDocument = productResponse.parse();
                //System.out.println(productDocument.body());

                Elements livre_con = productDocument.select(".livre_con");

                //System.out.println(livre_con);
                try {
                    Element img = livre_con.select("img").first();
                    String imageSrc = img.attr("src");
                    //System.out.println(imageSrc);
                    //System.out.println(imageSrc.contains("https") ? imageSrc : BABELIO_COM + imageSrc);
                    book.setImage(imageSrc.contains("https") ? imageSrc : BABELIO_COM + imageSrc);
                } catch (Exception e) {
                    System.out.println("isbn problem: " + isbn);
                    e.printStackTrace();
                }

                try {
                    Element first = productDocument.select("#d_bio").first();
                    String bio = first.html();
                    String description = bio.replaceAll("\\<span.*?\\>.*?\\<\\/span\\>", "").replaceAll("<p class=\"footer\" style=\"clear:both;border:0px;\"></p>", "");
                    int lastIndexDot = description.lastIndexOf("...");
                    int length = description.length();
                    if (lastIndexDot<length-10){
                        book.setDesc(description);
                    } else {
                        String substring = description.substring(0, lastIndexDot);
                        lastIndexDot = substring.lastIndexOf(".");
                        book.setDesc(substring.substring(0, lastIndexDot));
                    }

                } catch (Exception e) {
                    System.out.println("isbn problem: " + isbn);
                    e.printStackTrace();
                }

            }
            //System.out.println(bio);
        } catch (IOException e) {
            System.out.println("isbn problem: " + isbn);
            e.printStackTrace();
        }


        return book;
    }

    static String getUserAgent(String isbn) {
        //int isbnNumber = Integer.parseInt(isbn);

        //Long decodedLong = Long.decode(isbn);
        String chrome_80 = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";
        String andr = "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
        return Long.decode(isbn) % 2 == 0 ? chrome_80 : andr;
    }
}
